package logica;

import java.util.Queue;
import java.util.Stack;

public class ArbolExpresion {
	
	public Nodo raiz;

	public static class Nodo {
		public String val;
		public Nodo izq, der;
		public Nodo(String v) { val = v; }
	}
	
	//Genera un Arbol de expresion. Hojas con operandos, y Padres con operadores
	static ArbolExpresion generarArbol(Queue<String> RPN){
		
		Stack<ArbolExpresion> pilaArbol = new Stack<>();
		
		for(String elemento: RPN){
			if((AntiCalculadora.esUnOperador(elemento))){
				ArbolExpresion expresion = new ArbolExpresion(elemento);
				expresion.insertarDer(pilaArbol.pop().raiz);
				expresion.insertarIzq(pilaArbol.pop().raiz);
				pilaArbol.push(expresion);
			}
			else{
				ArbolExpresion operando = new ArbolExpresion(elemento);
				pilaArbol.push(operando);
			}
		}
		
		return pilaArbol.peek();
	}
	//Resuelve un ArboldeExpresion, generando otro arbol en el cual los Padres contienen el resultado de sus hijos.
	static ArbolExpresion resolverArbol(ArbolExpresion arbol){
		
		ArbolExpresion ret = new ArbolExpresion("");
		ret.raiz = resolverArbol(arbol.raiz);
		
		return ret;
	}
	
	static Nodo resolverArbol(Nodo p){
		if(p==null){
			return p;
		}
		if(AntiCalculadora.esUnOperador(p.val)){
			p.val = Integer.toString(AntiCalculadora.evaluar(p.val,resolverArbol(p.der).val,resolverArbol(p.izq).val));
		}
		return p;
	}
	
	public ArbolExpresion(String elemento){
		this.raiz = new Nodo(elemento);
	}
	
	public void insertarIzq(Nodo arbol){
		this.raiz.izq = arbol;
	}
	
	public void insertarDer(Nodo arbol){
		this.raiz.der = arbol;
	}
	
	public String toString() {
		return "AB<" + toString(raiz) + ">";
	}
	
	private String toString(Nodo p) {
		if (p == null)
			return "\u2205";  // Simbolo de vacio.

		String val = p.val;
		if (esHoja(p))
			return val;  // La hoja se representa asi misma.

		return "(" + val + " " + toString(p.izq) + " " + toString(p.der) + ")";
	}

	protected boolean esHoja(Nodo p) {
		return (p.izq == null && p.der == null);
		 
	}
	
	
	
	
}
