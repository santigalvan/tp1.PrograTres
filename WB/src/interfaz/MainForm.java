package interfaz;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;

import logica.AntiCalculadora;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

public class MainForm{

	JFrame frameAntiCalculadora;
	AntiCalculadora logica;
	
	//Actualmente no sabemos cuandos campos de texto vamos a necesitar, 
	//por eso creamos un array y luego en base a los operadores vamos a definirlo.

	private JTextField[] camposNumericos;
	private JTextField[] camposOperadores;
	private JTextField[] parentesis;
	private JTextField resultado;
	private JTextField seleccionActual;

	
	private JButton[] botonesOperadores;
	private JButton[] padNumerico;
	//Una vez que tengas tu resultado, pulsando este bot�n podras ver si tu respuesta es correcta.
	private JButton enviar;
	private JButton borrar;
	//Cada vez que presiones este bot�n, pasar�s al siguiente cuadro de texto, para seguir completando tu respuesta.
	private JButton siguiente;
	private JButton anterior;
	
	private int[] posicionParentesis;
	private int seleccion; 
	private int numeroDeNivel; 

	//Pad n�merico, con el objetivo de limitar en cierto modo a que el jugador no envie informaci�n errone
	/**
	 * Create the application.
	 */
	public MainForm(int nNivel) {
		numeroDeNivel = nNivel;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		logica = new AntiCalculadora(numeroDeNivel);

		//Seleccionamos el primer cuadro por defecto
		seleccion = 0;
		
		posicionParentesis = new int[2];
		parentesis = new JTextField[2];
		camposNumericos = new JTextField[logica.cantOperadores() + 1];
		camposOperadores = new JTextField[logica.cantOperadores()];
		
		botonesOperadores = new JButton[logica.cantOperadores()];
		
		padNumerico = new JButton[10];
		
		frameAntiCalculadora = new JFrame();
		frameAntiCalculadora.getContentPane().setEnabled(true);
		frameAntiCalculadora.getContentPane().setFocusable(true);
		frameAntiCalculadora.getContentPane().setBackground(new Color(160,160,160));
		frameAntiCalculadora.setTitle("Anti-Calculadora");
		frameAntiCalculadora.setBounds(100, 100, 700, 500);
		frameAntiCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Campos donde se mostraran los operadores ingresados por el usuario
		crearCamposOperadores();
		//Campos donde ir�n los n�meros ingresados por el usuario.
		crearCamposNumericos();
		//Cuadro verde para indicar al usuario d�nde est� editando actualmente
		crearCuadroSeleccion();
		//Medio por el cual el usuario ingresa los operadores a la ecuaci�n
		crearBotonesOperadores();
		//Medio por el cual el usuario ingresa los numeros en los campos
		crearPadNumerico();
		//En el caso de que el nivel de dificultad sea elevado, se dibujaran parentesis en la pantalla
		crearParentesis();
		//Detalle est�tico 
		crearLineasSeparadoras();		

		//Puramente para orientar las posiciones y dejarlo lo mas simetrico posible
		//reglasHorizontales();	
		//reglasVerticales();	
	
		resultado = new JTextField(logica.obtenerResultado());
		resultado.setBounds(290, 30, 100, 60);
		resultado.setEditable(false);
		resultado.setForeground(new Color(255, 255, 255));
		resultado.setHorizontalAlignment(SwingConstants.CENTER);
		resultado.setFont(new Font("Consolas", Font.PLAIN, 35));
		resultado.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		resultado.setBackground(new Color(0, 191, 255));
		
		frameAntiCalculadora.getContentPane().add(resultado);
		resultado.setColumns(10);
		
		siguiente = new JButton(">>");
		siguiente.setBounds(570, 198, 70, 35);
		siguiente.setForeground(Color.WHITE);
		siguiente.setFont(new Font("Consolas", Font.PLAIN, 20));
		siguiente.setBackground(new Color(0, 191, 255));
		frameAntiCalculadora.getContentPane().add(siguiente);
		
		anterior = new JButton("<<");
		anterior.setBounds(50, 198, 70, 35);
		anterior.setForeground(Color.WHITE);
		anterior.setFont(new Font("Consolas", Font.PLAIN, 20));
		anterior.setBackground(new Color(0, 191, 255));
		frameAntiCalculadora.getContentPane().add(anterior);
				
		borrar = new JButton("C");
		borrar.setBounds(570, 276, 70, 80);
		borrar.setForeground(Color.WHITE);
		borrar.setFont(new Font("Consolas", Font.PLAIN, 20));
		borrar.setBackground(new Color(255, 87, 51));
		frameAntiCalculadora.getContentPane().add(borrar);

		enviar = new JButton("OK!");
		enviar.setBounds(50, 276, 70, 80);
		enviar.setForeground(Color.WHITE);
		enviar.setFont(new Font("Consolas", Font.PLAIN, 20));
		enviar.setBackground(new Color(66, 255, 51));
		frameAntiCalculadora.getContentPane().add(enviar);
		enviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		enviar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(camposCompletos()){
					Final pantallaDevolucion = new Final(enviarEcuacion(), resultado.getText(), frameAntiCalculadora, logica);
					pantallaDevolucion.frameFinal.setVisible(true);
					frameAntiCalculadora.setVisible(false);
				}
				else{
					JOptionPane.showMessageDialog(null,"Campos incompletos");
				}
			}
		});
		
		JLabel mascota = new JLabel();
		mascota.setBounds(10, 11, 80, 80);
		mascota.setIcon(new ImageIcon(MainForm.class.getResource("/Imagenes/cosita.gif")));
		frameAntiCalculadora.getContentPane().add(mascota);
		
		JLabel lblNivel = new JLabel("Nivel: "+numeroDeNivel);
		lblNivel.setForeground(Color.WHITE);
		lblNivel.setFont(new Font("Consolas", Font.BOLD, 30));
		lblNivel.setBounds(518, 11, 166, 35);
		frameAntiCalculadora.getContentPane().add(lblNivel);
		
		//Capta acci�n del boton "C", el cual limpia el campo n�merico de la seleccion actual (Cuadro verde)
		listenerBorrar();
		//Capta la acci�n de los botones por los cuales el usuario agrega los operadores a la ecuaci�n
		listenerBotonesOperadores();
		//Medio por el cual el usuario ingresa los numeros (Restricci�n: Por cuestiones de dise�o, �ste n�mero no puede
		//Tener una longitud mayor que dos.
		listenerPadNumerico();
		//En el caso de querer seleccionar los campos n�mericos por evento del mouse, haciendo clic, la seleccion actual 
		//Se mueve hacia ese cuadro seleccionado
		listenerCamposNumericos();
		//Toma acci�n del bot�n ">>" y avanza a la siguiente posici�n en caso de ser posible
		listenerSiguiente();
		//Toma acci�n del bot�n "<<" y retrocede a la anterior posici�n en caso de ser posible
		listenerAnterior();
	}

	/**METODOS DE DIBUJADO EN PANTALLA**/
	//Con esto, creamos los botones que van a ser visualizados por el usuario

	private void crearCamposOperadores(){
		int espacioAgregado  = espacioInicialOperadores(); //Variable para espaciar entre cuadro y cuadro por cada iteracion
		int anchoVentana = frameAntiCalculadora.getWidth();
		
		for(int i=0; i<camposOperadores.length; i++){
			camposOperadores[i] = new JTextField();
			camposOperadores[i].setHorizontalAlignment(SwingConstants.CENTER);
			camposOperadores[i].setFont(new Font("Consolas", Font.PLAIN, 35));
			camposOperadores[i].setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			camposOperadores[i].setEditable(false);
			camposOperadores[i].setBackground(Color.LIGHT_GRAY);
			camposOperadores[i].setBounds(anchoVentana/2 + espacioAgregado , 128, 35, 35);
			
			espacioAgregado  = espacioAgregado + 146;
			
			frameAntiCalculadora.getContentPane().add(camposOperadores[i]);
			camposOperadores[i].setColumns(10);
		}
	}
	//Creamos y ubicamos los cuadros en la aplicaci�n-
	private void crearCamposNumericos(){
		int anchoVentana = frameAntiCalculadora.getWidth();
		int espacioAgregado = espacioInicialCuadros();

		for(int i=0; i<camposNumericos.length; i++){
			camposNumericos[i] = new JTextField();
			camposNumericos[i].setHorizontalAlignment(SwingConstants.CENTER);
			camposNumericos[i].setFont(new Font("Consolas", Font.PLAIN, 40));
			camposNumericos[i].setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			camposNumericos[i].setEditable(false);
			camposNumericos[i].setBackground(Color.LIGHT_GRAY);
			camposNumericos[i].setBounds((anchoVentana/2) - 20 + espacioAgregado, 115, 65, 60);
			
			espacioAgregado = espacioAgregado + 146;
			
			frameAntiCalculadora.getContentPane().add(camposNumericos[i]);
			camposNumericos[i].setColumns(10);
		}
	}
	
	private void crearCuadroSeleccion() {
		int espacioInicial = espacioInicialCuadros();
		frameAntiCalculadora.getContentPane().setLayout(null);
		seleccionActual = new JTextField();
		seleccionActual.setBounds(frameAntiCalculadora.getWidth()/2-25 + espacioInicial, 110, 75, 70);
		seleccionActual.setEditable(false);
		seleccionActual.setBackground(new Color(66, 255, 51));
		frameAntiCalculadora.getContentPane().add(seleccionActual);
		seleccionActual.setColumns(10);
	}

	private void crearBotonesOperadores(){
		int espacioAgregado = espacioInicialBOperadores();
		int anchoVentana = frameAntiCalculadora.getWidth();
		String operadores = logica.obtenerOperadores();
		
		for (int i = 0; i < logica.cantOperadores(); i++) {
			botonesOperadores[i] = new JButton(String.valueOf(operadores.charAt(i)));	
			botonesOperadores[i].setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
			botonesOperadores[i].setBackground(new Color(66, 255, 51));
			botonesOperadores[i].setVerticalAlignment(1);
			botonesOperadores[i].setForeground(Color.white);
			botonesOperadores[i].setFont(new Font("Consolas", Font.PLAIN, 30));
			botonesOperadores[i].setBounds(anchoVentana/2 + espacioAgregado, 198, 40, 34);
			frameAntiCalculadora.getContentPane().add(botonesOperadores[i]);
			
			espacioAgregado += 80;
		}
	}
	
	private void crearPadNumerico(){
		int espacioAgregadoX = 0;
		int espacioAgregadoY = 0;
			
		for(int i=0; i<padNumerico.length-1; i++){
			padNumerico[i] = new JButton(String.valueOf(i+1));
			padNumerico[i].setFont(new Font("Consolas", Font.PLAIN, 10));
			padNumerico[i].setForeground(new Color(0, 0, 0));
			padNumerico[i].setBounds(frameAntiCalculadora.getWidth()/2 - 89 + espacioAgregadoX, 258 + espacioAgregadoY, 40, 35);
			frameAntiCalculadora.getContentPane().add(padNumerico[i]);
			
			espacioAgregadoX += 60;
			
			if((i+1)%3==0){
				espacioAgregadoX = 0;
				espacioAgregadoY += 50;
			}
		}
		padNumerico[9] = new JButton("0");
		padNumerico[9].setFont(new Font("Consolas", Font.PLAIN, 10));
		padNumerico[9].setForeground(new Color(0, 0, 0));
		padNumerico[9].setBounds(frameAntiCalculadora.getWidth()/2 - 29, 406, 40, 35);
		frameAntiCalculadora.getContentPane().add(padNumerico[9]);
	}

	private void crearParentesis() {
		if(logica.cantOperadores() > 2) {
			Random rnd = new Random();
			int random = rnd.nextInt(camposOperadores.length-1);
			
			posicionParentesis[0] = random;
			parentesis[0] = new JTextField("(");
			parentesis[0].setBorder(null);
			parentesis[0].setOpaque(false);
			parentesis[0].setHorizontalAlignment(SwingConstants.CENTER);
			parentesis[0].setFont(new Font("Consolas", Font.PLAIN, 50));
			parentesis[0].setEditable(false);
			parentesis[0].setBounds(camposNumericos[random].getX()-25, camposNumericos[random].getY(), 30, 60);
				
			frameAntiCalculadora.getContentPane().add(parentesis[0]);
			random = random + rnd.nextInt(2)+1;

			posicionParentesis[1] = random;
			parentesis[1] = new JTextField(")");
			parentesis[1].setBorder(null);
			parentesis[1].setOpaque(false);
			parentesis[1].setHorizontalAlignment(SwingConstants.CENTER);
			parentesis[1].setFont(new Font("Consolas", Font.PLAIN, 50));
			parentesis[1].setEditable(false);
			parentesis[1].setBounds(camposNumericos[random].getX()+60, camposNumericos[random].getY(), 30, 60);
			
			frameAntiCalculadora.getContentPane().add(parentesis[1]);
		}
	}

	private void crearLineasSeparadoras() {
		JSeparator separadorHorizontal_0 = new JSeparator();
		separadorHorizontal_0.setBorder(null);
		separadorHorizontal_0.setBounds(0, 238, 700, 2);
		separadorHorizontal_0.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorHorizontal_0);
	
		JSeparator separadorHorizontal_1 = new JSeparator();
		separadorHorizontal_1.setBorder(null);
		separadorHorizontal_1.setBounds(0, 190, 700, 2);
		separadorHorizontal_1.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorHorizontal_1);
		
		JSeparator separadorHorizontal_2 = new JSeparator();
		separadorHorizontal_2.setBorder(null);
		separadorHorizontal_2.setBounds(0, 99, 700, 2);
		separadorHorizontal_2.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorHorizontal_2);
		
		JSeparator separadorVertical_0 = new JSeparator();
		separadorVertical_0.setBorder(null);
		separadorVertical_0.setBounds(135, 190, 2, 50);
		separadorVertical_0.setOrientation(SwingConstants.VERTICAL);
		separadorVertical_0.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorVertical_0);
		
		JSeparator separadorVertical_1 = new JSeparator();
		separadorVertical_1.setBorder(null);
		separadorVertical_1.setBounds(552, 190, 2, 50);
		separadorVertical_1.setOrientation(SwingConstants.VERTICAL);
		separadorVertical_1.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorVertical_1);
		
		JSeparator separadorVertical_2 = new JSeparator();
		separadorVertical_2.setBorder(null);
		separadorVertical_2.setBounds(240, 240, 2, 260);
		separadorVertical_2.setOrientation(SwingConstants.VERTICAL);
		separadorVertical_2.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorVertical_2);
		
		JSeparator separadorVertical_3 = new JSeparator();
		separadorVertical_3.setBorder(null);
		separadorVertical_3.setBounds(436, 240, 2, 260);
		separadorVertical_3.setOrientation(SwingConstants.VERTICAL);
		separadorVertical_3.setBackground(new Color(0, 0, 0));
		frameAntiCalculadora.getContentPane().add(separadorVertical_3);
		
	}

	/**METODOS DE ACCIONES**/

	//Agrego un listener para que tome el evento si algun boton es presionado.
		

	private void listenerAnterior(){
	anterior.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent evento) {
				cambiarPosicion("volver");
			}
		});
	}
	
	private void listenerSiguiente(){
		siguiente.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evento) {
					cambiarPosicion("avanzar");
				}
			});
	}
	
	private void listenerBorrar(){
		borrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evento) {
					limpiarCampo();
				}
			});
	}
	
	private void limpiarCampo(){
		camposNumericos[seleccion].setText("");
	}
	
	private void listenerCamposNumericos(){
		for(int i=0; i<camposNumericos.length; i++) {
			camposNumericos[i].addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					for (int i = 0; i < camposNumericos.length; i++) {
						if(arg0.getSource() == camposNumericos[i]){
							actualizarSeleccion(i);
						}
					}
				}
			});
		}
	}

	private void listenerPadNumerico(){
		for(int i=0; i<padNumerico.length; i++) {
			padNumerico[i].addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent event) {
					for (int i = 0; i < padNumerico.length; i++) {
						if(event.getSource() == padNumerico[i]){
							escribirCuadro(padNumerico[i].getText());
						}
					}
				}
			});
		}
	}

	//Toma la accion aplicada por eventos de mouse sobre los botones de los operadores
	private void listenerBotonesOperadores() {
		for(int i=0; i<botonesOperadores.length; i++) {
			botonesOperadores[i].addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					for (int i = 0; i < botonesOperadores.length; i++) {
						if(arg0.getSource() == botonesOperadores[i]){
							
							if(obtenerColorBoton(botonesOperadores[i]).equals("Verde"))
								escribirCuadroOperador(botonesOperadores[i].getText());
							
							if(obtenerColorBoton(botonesOperadores[i]).equals("Rojo"))
								borrarOperador(botonesOperadores[i].getText());
							
							cambiarColorBoton(botonesOperadores[i]);
						}
					}
				}
			});
		}
	}

	/**METODOS ADICIONALES**/

	//Tomo la posicion del cuadro actual, avanzo o retrecedo dependiendo de la acci�n correspondiente
	private void cambiarPosicion(String accion) {
		if(accion.equals("avanzar") && seleccion < camposNumericos.length-1){
				seleccion++;
		}
		else if(accion.equals("volver") && seleccion > 0){
			seleccion --;
		}
		seleccionActual.setBounds(camposNumericos[seleccion].getX()-5, camposNumericos[seleccion].getY()-5, 75, 70);
	}

	private void actualizarSeleccion(int numeroDeCampo){
		seleccion = numeroDeCampo;
		seleccionActual.setBounds(camposNumericos[numeroDeCampo].getX()-5, camposNumericos[numeroDeCampo].getY()-5, 75, 70);
	}
	
	//Verifica que el usuario no envie campos vacios
	public boolean camposCompletos(){
		for (int i = 0; i < camposNumericos.length; i++) {
			if(camposNumericos[i].getText().isEmpty()){
				return false;
			}
		}
		for (int i = 0; i < camposOperadores.length; i++) {
			if(camposOperadores[i].getText().isEmpty()){
				return false;
			}
		}
		return true;
	}
	
	//Concatenamos los valores de los campos de texto y devolvemos la operacion a realizar
	private String enviarEcuacion(){
		String ret="";
		
		if(numeroDeNivel<5){
			posicionParentesis[0] = -1;
			posicionParentesis[1] = -1;		
		}
		
		for (int i = 0; i < camposNumericos.length; i++) {
			if(posicionParentesis[0] == i){
				ret+="(";
			}
			
			ret+=camposNumericos[i].getText();
			
			if(posicionParentesis[1] == i){
				ret+=")";
			}
			
			if(i<camposOperadores.length)
				ret+=camposOperadores[i].getText();
	  }
		return ret;
	}
	
	//Recorremos los campos donde se encuentran los operadores previamente ingresados por el usuario y limpiamos
	//el campo con respecto al boton del operador que se ha presionado
	private void borrarOperador(String operador) {
		for (int i = 0; i < camposOperadores.length; i++) {
			if(camposOperadores[i].getText().equals(operador)){
				camposOperadores[i].setText("");
				break;
			}
		}
	}
	//En la posici�n actual, se modifica el campo numerico si solo si, la longitud del campo actual es menor a dos
	private void escribirCuadro(String numeroBoton) {
		if(camposNumericos[seleccion].getText().length() < 2){
			camposNumericos[seleccion].setText(camposNumericos[seleccion].getText() + numeroBoton);
		}
	}
	
	//Ingresa el operador por el cual se ha llamado al evento y se lo ingresa en la primer posicion vac�a
	private void escribirCuadroOperador(String operador) {
		for(int i=0; i<camposOperadores.length; i++){
			if(camposOperadores[i].getText().equals("")){
				camposOperadores[i].setText(operador);
				break;
			}
		}
	}
	
	//En base a la cantidad de operadores, podemos distruibuir los cuadros
	//de tal manera que queden ordenados y bien distrubuidos.
	private int espacioInicialOperadores() {
		if(logica.cantOperadores() == 1)
			return -26;
		if(logica.cantOperadores() == 2)
			return -99;
		if(logica.cantOperadores() == 3)
			return -172;
		if(logica.cantOperadores() == 4)
			return -245;
		throw new RuntimeException("No se pudo iniciar el espacio de los operadores");
	}
	
	private int espacioInicialCuadros() {
		if(logica.cantOperadores() == 1)
			return -94;
		if(logica.cantOperadores() == 2)
			return -167;
		if(logica.cantOperadores() == 3)
			return -240;
		if(logica.cantOperadores() == 4)
			return -313;
		throw new RuntimeException("No se pudo iniciar el espacio de los cuadros");
	}
	
	private int espacioInicialBOperadores() {
		if(logica.cantOperadores() == 1)
			return -30;
		if(logica.cantOperadores() == 2)
			return -65;
		if(logica.cantOperadores() == 3)
			return -107;
		if(logica.cantOperadores() == 4)
			return -147;
		throw new RuntimeException("No se pudo iniciar el espacio de los botones");
	}
	
	//Solo pasa de verde (sin utilizar) a rojo (utilizado) y viceversa
	private void cambiarColorBoton(JButton boton) {
		int[] colorRGB = obtenerRGB(boton); //Contiene los colores RGB del boton para verificar el estado actual del mismo
		if(verificarColor(colorRGB).equals("Verde")) {
			boton.setBackground(new Color(255, 87, 51)); //Paso del color verde al rojo
			return;
		}
		boton.setBackground(new Color(66, 255, 51));//Si no es verde, entonces tengo que pasar al verde.
	}

	//Devuelve en las posiciones [0] = ROJO, [1] = VERDE, [2] = AZUL.
	private int[] obtenerRGB(JButton boton) {
		int[] ret = new int[3];
		
		ret[0] = boton.getBackground().getRed();
		ret[1] = boton.getBackground().getGreen();
		ret[2] = boton.getBackground().getBlue();
		
		return ret;
	}
	
	//Obtengo el color del boton en base a sus 
	private String obtenerColorBoton(JButton boton){
		int[] RGB = obtenerRGB(boton);
		return verificarColor(RGB);
	}
	
	//Solo aplicable a esta interface, asume que con tener la primer posicion con el valor 255 es Rojo
	//Y asume que con tener la segunda posici�n en 255 es Verde, por el simple hecho de que hay dos colores posibles.
	private String verificarColor(int[] colorRGB) {
		if(colorRGB[0] == 255)
			return "Rojo";
		if(colorRGB[1] == 255)
			return "Verde";
		throw new RuntimeException("No se pudo verificar el color");
	}
}
