package interfaz;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;

import logica.ArbolExpresion;
import logica.AntiCalculadora;
import logica.ArbolExpresion.Nodo;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Final {

	JFrame frameFinal;
	JFrame frameJuego;
	AntiCalculadora logica;
	Menu menu;
	
	private JButton menuPrincipal;
	private JButton siguienteNivel;
	private JButton reintentar;
	
	private String ecuacion;
	private String resultado;
	
	public Final(String ecuacionCandidata, String objetivoUsuario, JFrame juegoGuardado, AntiCalculadora logicaGuardada) {
		frameJuego = juegoGuardado;
		logica = logicaGuardada;
		ecuacion = ecuacionCandidata;
		resultado = objetivoUsuario;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameFinal = new JFrame();
		frameFinal.getContentPane().setBackground(new Color(160,160,160));
		frameFinal.setBounds(100, 100, 700, 500);
		frameFinal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameFinal.getContentPane().setLayout(null);
		
		crearBotonMenuPrincipal();
		crearResolucion();
		
		//reglasHorizontales();
		//reglasVerticales();
		
		if(logica.verificarSiGano(ecuacion, resultado)) {
			crearMensajeGanador();
			
			crearBotonSiguienteNivel();
			listenerBotonSiguienteNivel();
		}
		else {
			crearMensajePerdedor();
			
			crearBotonReintentar();
			listenerBotonReintentar();
		}
		
	}

	/**METODOS DE DIBUJADO EN PANTALLA**/
	private void crearBotonSiguienteNivel() {
		siguienteNivel = new JButton("Siguiente Nivel");
		siguienteNivel.setForeground(new Color(240, 248, 255));
		siguienteNivel.setFont(new Font("Consolas", Font.PLAIN, 17));
		siguienteNivel.setBackground(new Color(255, 87, 91));
		siguienteNivel.setBackground(new Color(50, 205, 50));
		siguienteNivel.setBounds(frameFinal.getWidth()/2-120, 320, 220, 50);
		frameFinal.getContentPane().add(siguienteNivel);
	}
	
	private void crearBotonMenuPrincipal() {
		menuPrincipal = new JButton("Menu Principal");
		menuPrincipal.setForeground(Color.WHITE);
		menuPrincipal.setBackground(Color.GRAY);
		menuPrincipal.setFont(new Font("Consolas", Font.PLAIN, 17));
		menuPrincipal.setBounds(frameFinal.getWidth()/2-120, 380, 220, 50);
		frameFinal.getContentPane().add(menuPrincipal);
		menuPrincipal.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				menuPrincipal.setBackground((Color.LIGHT_GRAY));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				menuPrincipal.setBackground(Color.GRAY);
			}
		});
		menuPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				menu = new Menu();
				menu.frameMenu.setVisible(true);
				frameFinal.setVisible(false);
			}
		});
	}
	
	private void crearBotonReintentar() {
		reintentar = new JButton("Reintentar");
		reintentar.setForeground(new Color(240, 248, 255));
		reintentar.setFont(new Font("Consolas", Font.PLAIN, 17));
		reintentar.setBackground(new Color(255, 87, 91));

		reintentar.setBounds(frameFinal.getWidth()/2-120, 320, 220, 50);
		frameFinal.getContentPane().add(reintentar);
	}

	private void crearResolucion(){
		ArbolExpresion arbolResolucion = logica.crearArbolExpresion(ecuacion);
		ArbolExpresion arbolMuestreo = logica.crearArbolExpresion(ecuacion);
		logica.resolverArbolExpresion(arbolMuestreo);
		crearExpresion();
		crearArbol(arbolResolucion.raiz, frameFinal.getWidth()/2 - 210, 200);
		crearArbol(arbolMuestreo.raiz, frameFinal.getWidth()/2 - 210, 200);
	}
	
	public void crearArbol(Nodo p, int x, int y){
		int espacioYAux = 0;
		if(p==null){
			return;
		}		
		if(AntiCalculadora.esUnOperador(p.val)) { //PREGUNTAR A GER
			espacioYAux = -50;
		}

		crearArbol(p.izq,x-70,y-50);
		crearArbol(p.der, x+70, y-50);
			
		JTextField valorActual = new JTextField(p.val);
		valorActual.setHorizontalAlignment(SwingConstants.CENTER);
		valorActual.setFont(new Font("Consolas", Font.PLAIN, 20));
		valorActual.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		valorActual.setEditable(false);
		valorActual.setBounds(185+x, 25+y+espacioYAux, 30, 30);
		frameFinal.getContentPane().add(valorActual);
		
	}

	private void crearExpresion() {
		JTextField ecuacionCompleta = new JTextField(ecuacion);
		ecuacionCompleta.setHorizontalAlignment(SwingConstants.CENTER);
		ecuacionCompleta.setFont(new Font("Consolas", Font.PLAIN, 25));
		ecuacionCompleta.setForeground(Color.white);
		ecuacionCompleta.setEditable(false);
		ecuacionCompleta.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		ecuacionCompleta.setBackground(new Color(0, 191, 255));
		ecuacionCompleta.setBounds(frameFinal.getWidth()/2 - 150, 20, 180, 40);
		frameFinal.getContentPane().add(ecuacionCompleta);
		
		JTextField igual = new JTextField("=");
		igual.setHorizontalAlignment(SwingConstants.CENTER);
		igual.setFont(new Font("Consolas", Font.PLAIN, 25));
		igual.setForeground(Color.white);
		igual.setEditable(false);
		igual.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		igual.setBackground(new Color(0, 191, 255));
		igual.setBounds(frameFinal.getWidth()/2+40, 20, 30, 40);
		frameFinal.getContentPane().add(igual);

		JTextField cuadroResultado = new JTextField(resultado+"?");
		cuadroResultado.setHorizontalAlignment(SwingConstants.CENTER);
		cuadroResultado.setFont(new Font("Consolas", Font.PLAIN, 25));
		cuadroResultado.setForeground(Color.white);
		cuadroResultado.setEditable(false);
		cuadroResultado.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		cuadroResultado.setBackground(new Color(0, 191, 255));
		cuadroResultado.setBounds(frameFinal.getWidth()/2+80, 20, 50, 40);
		frameFinal.getContentPane().add(cuadroResultado);
}	
	
	/**MENSAJES AL USUARIO**/
	private void crearMensajeGanador(){
		JLabel mensaje = new JLabel("Bien Hecho!");
		mensaje.setBackground(Color.GREEN);
		mensaje.setForeground(Color.GREEN);
		mensaje.setFont(new Font("Dialog", Font.PLAIN, 35));
		mensaje.setBounds(frameFinal.getWidth()/2 -105, 250, 320, 70);
		frameFinal.getContentPane().add(mensaje);
	}
	
	private void crearMensajePerdedor() {
		JLabel mensaje = new JLabel("Tuviste un error!");
		mensaje.setForeground(Color.RED);
		mensaje.setFont(new Font("Consolas", Font.PLAIN, 35));
		mensaje.setBounds(frameFinal.getWidth()/2 -170, 250, 600, 70);
		frameFinal.getContentPane().add(mensaje);
	}

	/**LISTENERS**/
	private void listenerBotonSiguienteNivel() {
		siguienteNivel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				siguienteNivel.setBackground(new Color(20, 255, 20));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				siguienteNivel.setBackground(new Color(50, 205, 50));
			}
		});
		siguienteNivel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainForm pantallaJuegoNueva = new MainForm(logica.obtenerNivelActual()+1);
				pantallaJuegoNueva.frameAntiCalculadora.setVisible(true);
				frameJuego.dispose();	
				frameFinal.setVisible(false);
			}
		});
	}

	private void listenerBotonReintentar() {
		reintentar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				reintentar.setBackground(new Color(255, 150, 127));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				reintentar.setBackground(new Color(255, 87, 91));
			}
		});
		reintentar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				///avanzarNivel ();
				frameJuego.setVisible(true);
				frameFinal.setVisible(false);
			}
		});
	}	
	
	/**METODOS ADICIONALES*/
	@SuppressWarnings("unused")
	private void reglasVerticales() {
		int altoVentana = frameFinal.getWidth();
		
		JSeparator mitadVertical = new JSeparator();
		mitadVertical.setBackground(Color.green);
		mitadVertical.setOrientation(SwingConstants.VERTICAL);
		mitadVertical.setBounds(altoVentana/2 - 10, 0, 2, 550);
		frameFinal.getContentPane().add(mitadVertical);	
		
		JSeparator primeraMitadVertical = new JSeparator();
		primeraMitadVertical.setBackground(Color.green);
		primeraMitadVertical.setOrientation(SwingConstants.VERTICAL);
		primeraMitadVertical.setBounds(altoVentana/4 - 10, 0, 2, 550);
		frameFinal.getContentPane().add(primeraMitadVertical);	

		JSeparator segundaMitadVertical = new JSeparator();
		segundaMitadVertical.setBackground(Color.green);
		segundaMitadVertical.setOrientation(SwingConstants.VERTICAL);
		segundaMitadVertical.setBounds(altoVentana/2 + altoVentana/4 - 10, 0, 2, 550);
		frameFinal.getContentPane().add(segundaMitadVertical);
	}

	@SuppressWarnings("unused")
	private void reglasHorizontales() {
		int anchoVentana = frameFinal.getHeight();
		
		JSeparator mitadHorizontal = new JSeparator();
		mitadHorizontal.setBackground(Color.red);
		mitadHorizontal.setBounds(0, anchoVentana/2 - 20, 600, 2);
		frameFinal.getContentPane().add(mitadHorizontal);	
		
		JSeparator primeraMitadHorizontal = new JSeparator();
		primeraMitadHorizontal.setBackground(Color.red);
		primeraMitadHorizontal.setBounds(0, (anchoVentana/2- 20) /2, 600, 2);
		frameFinal.getContentPane().add(primeraMitadHorizontal);	
				
		JSeparator segundaMitadHorizontal = new JSeparator();
		segundaMitadHorizontal.setBackground(Color.red);
		segundaMitadHorizontal.setBounds(0, (anchoVentana/2) + anchoVentana/4 - 20, 600, 2);
		frameFinal.getContentPane().add(segundaMitadHorizontal);
	}
}
